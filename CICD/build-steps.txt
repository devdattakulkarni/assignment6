echo "This is a build step"

cd Containers/Kubernetes-examples/GCP/greetings
docker build -t gcr.io/cloudark-test-gke/greetings .
gcloud docker -- push gcr.io/cloudark-test-gke/greetings

echo "SUCCESS"
